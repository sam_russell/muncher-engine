#ifndef RENDERLOOP_HPP
#define RENDERLOOP_HPP

#include <cmath>
#include <iostream>
#include <vector>
#include <stack>
#include <utility>
#include <deque>
#include <list>
#include <unordered_map>
#include <set>

#include <SFML/Graphics.hpp>

#include "Event.hpp"
#include "Renderable.hpp"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#endif

namespace muncher {

  /* ALREADY DEFINED IN EVENT_HPP
  enum DefaultRenderTypes
    {
     CIRCLE = 1,
     RECTANGLE = 2,
     CHAIN = 3
    };
  */
  
  class RenderLoop
  {
  private:
    sf::RenderWindow* m_window;
    std::queue<QueryResponse> m_entities;
    std::unordered_map<int, std::set<int> > m_render_types;
    std::unordered_map<int, Renderable*> m_renderables;
    RenderableFactory* m_fac;
    void listen();
    float m_scale;
  public:
    RenderLoop(sf::RenderWindow*,
	       std::unordered_map<int, RenderFunctor*>,
	       float // scale
	       );
    sf::Transform get_transform();
    std::vector<Renderable*> get_renderables(int[]);
    void delete_renderable(int);
    void create_renderable(int);
    void create_renderable(int, int, std::vector<sf::Vector2f>);
    void create_renderable(int, int, std::vector<float>);
  };

  RenderLoop::RenderLoop(sf::RenderWindow* w,
			 std::unordered_map<int, RenderFunctor*> funcs,
			 float scale)
  {
    m_window = w;
    m_scale = scale;
    RenderableFactory* fac = new RenderableFactory(m_window,
						   this->get_transform(),
						   funcs);
    m_fac = fac;
  }

  sf::Transform RenderLoop::get_transform()
  {
    sf::Transform T;
    T.rotate (180.0f);
    T.scale(-1 * m_scale, m_scale);
    return T;
  }

  void RenderLoop::create_renderable(int id,
				     int f_key,
				    std::vector<sf::Vector2f> pos)
  {
    Renderable* r = m_fac->make_renderable(f_key, pos);
    m_renderables[id] = r;
  }

  std::vector<Renderable*> RenderLoop::get_renderables(int ids[])
  {
    std::vector<Renderable*> output;
    for (int i = 0; i < ARRAY_SIZE(ids); ++i)
      {
	int id = ids[i];
	Renderable* r = m_renderables[id];
	output.push_back(r);
      }
    return output;
  }
}

#endif
