#ifndef RENDERFUNCTOR_HPP
#define RENDERFUNCTOR_HPP

#include <iostream>
#include <deque>

#include <SFML/Graphics.hpp>

#include "Manifold.hpp"


namespace muncher
{
  class RenderFunctor
  {
  public:
    ~RenderFunctor () {}
    virtual sf::Drawable* operator() () = 0;
    virtual sf::Drawable* operator() (std::vector<float>) = 0;
    virtual sf::Drawable* operator() (std::vector<sf::Vector2f>) = 0;
  };
  

  class CircleFunctor: public RenderFunctor
  {
  private:
    float m_radius;
  public:
    CircleFunctor(float radius)
      : m_radius{radius}
    { /* NOP */ }
    virtual sf::Drawable* operator() (std::vector<sf::Vector2f>) {}
    virtual sf::Drawable* operator() (std::vector<float>) {}
    virtual sf::Drawable* operator() ();
  };

  sf::Drawable* CircleFunctor::operator() ()
  {
    sf::CircleShape* c = new sf::CircleShape(m_radius);
    c->setFillColor(sf::Color::Green);
    return c;
  }
  
  

  class RectangleFunctor: public RenderFunctor
  {
  private:
    float m_w;
    float m_h;
  public:
    RectangleFunctor(float, float);
    RectangleFunctor(sf::Vector2f);
    sf::Drawable* operator() ();
    virtual sf::Drawable* operator() (std::vector<float>) {}
    virtual sf::Drawable* operator() (std::vector<sf::Vector2f>) {}
  };
  RectangleFunctor::RectangleFunctor(float w, float h)
  {
    m_w = w;
    m_h = h;
  }
  RectangleFunctor::RectangleFunctor(sf::Vector2f wh)
  {
    m_w = wh.x;
    m_h = wh.y;
  }

  sf::Drawable* RectangleFunctor::operator() ()
  {
    sf::Vector2f size = sf::Vector2f(m_w, m_h);
    sf::RectangleShape* r = new sf::RectangleShape(size);
    r->setFillColor(sf::Color::Green);
    return r;
  }


  class ChainVertexArrayFunctor: public RenderFunctor
  {
  private:
    float m_thick;
  public:
    ChainVertexArrayFunctor(float thickness)
      : m_thick{thickness}
    { /* NOP */ }
    virtual sf::Drawable* operator() () {};
    virtual sf::Drawable* operator() (std::vector<float>) {}
    virtual sf::Drawable* operator() (std::vector<sf::Vector2f>);
  };

  sf::Drawable* ChainVertexArrayFunctor::operator()
    (std::vector<sf::Vector2f> ch)
  {
    std::deque<Manifold> manifolds = make_manifolds(ch, m_thick);
    sf::VertexArray a;
    a.append( sf::Vertex(manifolds[0].v0) );
    a.append( sf::Vertex(manifolds[0].v1) );

    a[0].color = sf::Color::Green;
    a[1].color = sf::Color::Blue;

    for (int i = 0; i < manifolds.size(); ++i)
      {
	if (i == manifolds.size() - 1)
	  {
	    std::cout << "last one" << std::endl;
	    sf::Vertex hi = sf::Vertex(manifolds[i].v2);
	    sf::Vertex lo = sf::Vertex(manifolds[i].v3);
	    
	    hi.color = sf::Color::Green;
	    lo.color = sf::Color::Blue;
	    
	    a.append(hi);
	    a.append(lo);
	  }
	else
	  {
	    //	    std::cout << i << std::endl;
	    float L = manifolds[i].normalDir;
	    float R = PI - manifolds[i + 1].normalDir;
	    
	    float sin_t = ( sin(L) * cos(R) ) - ( cos(L) * sin(R) );
	    float cos_t = ( cos(L) * cos(R) ) + ( sin(L) * sin(R) );
	    
	    float x = manifolds[i].v2.x;
	    float y = manifolds[i].v2.y;
	    
	    //float theta = acos(cos_t);
	    //float tangent = tan(theta);
	    float secant = 1/cos_t;
	    
	    float dx = sin_t * m_thick;
	    float dy = cos_t * m_thick;
	    
	    float lo_x;
	    float lo_y;
	    
	    float degL = L * (180.0f / PI);
	  
	    // lo_x = x - dx / secant;
	    lo_y = y - dy * secant;
	    
	    if (manifolds[i + 1].v2.y < manifolds[i].v2.y)
	      {
		lo_x = x + dx * secant;
	      }
	    else if (manifolds[i + 1].v2.y == manifolds[i].v2.y
		     &&
		     i + 1 == manifolds.size() - 1)
	      {
		lo_x = x + dx * secant;
	      }
	    else
	      {
		lo_x = x - dx / secant;
	      }
	  	  
	    sf::Vertex hi = sf::Vertex(sf::Vector2f(x, y));
	    sf::Vertex lo = sf::Vertex(sf::Vector2f(lo_x, lo_y));

	    hi.color = sf::Color::Green;
	    lo.color = sf::Color::Blue;

	    a.append(hi);
	    a.append(lo);
	  }

      }
    
    a.setPrimitiveType(sf::TriangleStrip);

    sf::VertexArray* new_array = new sf::VertexArray;
    new_array = &a;
    return new_array;
  }


}

#endif
