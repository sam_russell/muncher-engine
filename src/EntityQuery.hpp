#ifndef ENTITYQUERY_HPP
#define ENTITYQUERY_HPP

#include <vector>
#include <set>
#include <list>
#include <map>

#include "Box2D/Box2D.h"
#include <SFML/Graphics.hpp>

namespace muncher
{

  enum ResultProperty
    {
     POSITION = 1,
     ANGLE = 2,
     ANGULAR_VELOCITY = 3,
     LINEAR_VELOCITY = 4
    };

  struct EntityQueryResult
  {
    enum ResultProperty property;
    union
    {
      float angle;
      sf::Vector2f position;
      float angular_velocity;
      sf::Vector2f linear_velocity;
    };
  };


  class QueryFunctor
  {
  public:
    virtual EntityQueryResult operator() (b2Body*) = 0;
    /*    virtual float operator() ();
	  virtual bool operator() (); */
    QueryFunctor() {}
    virtual ~QueryFunctor();
  };
  /* virtual */
  QueryFunctor::~QueryFunctor() {delete this;}

  class PositionFunctor: public QueryFunctor
  {
  public:
    //PositionFunctor() {}
    virtual EntityQueryResult operator () (b2Body*);
  };
  /* virtual */
  EntityQueryResult PositionFunctor::operator() (b2Body* b)
  {
    b2Vec2 pos = b->GetPosition();
    EntityQueryResult r {};
    r.property = POSITION;
    r.position = sf::Vector2f(pos.x, pos.y);
    return r;
  }

  class AngleFunctor: public QueryFunctor
  {
  public:
    AngleFunctor() {}
    virtual EntityQueryResult operator () (b2Body*);
  };
  /* virtual */
  EntityQueryResult AngleFunctor::operator() (b2Body* b)
  {
    float ang = b->GetAngle();
    EntityQueryResult r {ANGLE, ang};
    return r;
  }


  class EntityQuery
  {
  private:
    b2World* m_world;
    std::vector<QueryFunctor*> m_queries;
    std::vector<EntityQueryResult> m_results;
  public:
    EntityQuery(b2World* world,
		std::vector<QueryFunctor*> queries)
      : m_world{world}, m_queries{queries}
    { /* NOP */ }
    ~EntityQuery();
    EntityQuery* execute(int);
    EntityData results();
    // std::map<int, EntityQueryResult> operator () (int[]);
  };

  EntityQuery::~EntityQuery()
  {
    for (std::vector<QueryFunctor*>::iterator func = m_queries.begin();
		 func != m_queries.end();
		 func++)
      {
	delete *func;
      }
  }

  EntityData EntityQuery::results()
  {
    EntityData entity_data {0};
    for (std::vector<EntityQueryResult>::iterator r = m_results.begin();
	 r != m_results.end();
	 r++)
      {
	int prop = r->property;
	switch (prop)
	  {
	  case POSITION:
	    entity_data.position = r->position;
	    break;
	  case ANGULAR_VELOCITY:
	    entity_data.angular_velocity = r->angular_velocity;
	    break;
	  case ANGLE:
	    entity_data.angle = r->angle;
	    break;
	  case LINEAR_VELOCITY:
	    entity_data.linear_velocity = r->linear_velocity;
	    break;
	  }
      }
    return entity_data;
  }
  
  EntityQuery* EntityQuery::execute(int id)
  {
    for (b2Body* b = m_world->GetBodyList(); b; b = b->GetNext() )
      {
	int* entity_id = (int*) b->GetUserData();
	std::cout << "entity_id: " << *entity_id << std::endl;
	if (*entity_id == id)
	  {
	    for (std::vector<QueryFunctor*>::iterator func = m_queries.begin();
	      	 func != m_queries.end();
		 func++)
	      {
		std::cout << "calling functor" << std::endl;
		EntityQueryResult result = (**func)(b);
		std::cout << "obtained result data" << std::endl;
		m_results.push_back(result);
	      }
	    std::cout << "returning results" << std::endl;
	    return this;
	  }
      }
    std::cout << "returning empty result set" << std::endl;
    return this;
  }


  QueryFunctor* GetQueryFunctor(int func_label)
  {
    //QueryFunctor* f;
    switch (func_label)
      {
      case POSITION:
	//PositionFunctor* pos_f = new PositionFunctor;
	//f = (QueryFunctor*) pos_f;
	return (QueryFunctor*) new PositionFunctor;
	break;
      case ANGLE:
	//AngleFunctor* ang_f = new AngleFunctor;
	//f = (QueryFunctor*) ang_f;
	return (QueryFunctor*) new PositionFunctor;
	break;
      }
  }


  struct QueryResponse
  {
    long mtype;
    EntityData result;
  };  
  

}

#endif
