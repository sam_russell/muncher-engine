#ifndef RENDERABLE_HPP
#define RENDERABLE_HPP

#include <unordered_map>
#include <vector>

#include <SFML/Graphics.hpp>

#include "RenderFunctor.hpp"

namespace muncher
{

  /* forward declaration */
  //  class RenderFunctor {};
  
  class Renderable
  {
  private:
    sf::RenderTarget* m_target;
    sf::Drawable* m_drawable;
    sf::RenderStates* m_states;
  public:
    Renderable(sf::RenderTarget* target,
	       sf::RenderStates* states,
	       sf::Drawable* drawable)
      : m_target{target},
	m_states{states},
	m_drawable{drawable}
    { /* NOP */ }
    void draw();
  };
  
  void Renderable::draw()
  {
    m_target->draw(*m_drawable, *m_states);
  }


  class RenderableFactory
  {
  private:
    sf::RenderWindow* m_window;
    sf::RenderStates m_states;
    std::unordered_map<int, RenderFunctor*> m_functors;
  public:
    RenderableFactory(sf::RenderWindow*,
		      sf::Transform,
		      std::unordered_map<int, RenderFunctor*>);
    Renderable* make_renderable(int);
    Renderable* make_renderable(int, std::vector<float>);
    Renderable* make_renderable(int, std::vector<sf::Vector2f>);
  };

  RenderableFactory::RenderableFactory(sf::RenderWindow* w,
				       sf::Transform t,
				       std::unordered_map<int, RenderFunctor*> m)
  {
    m_window = w;
    m_states.transform = t;
    m_functors = m;
  }

  Renderable* RenderableFactory::make_renderable(int t)
  {
    RenderFunctor* func = m_functors[t];
    sf::Drawable* d = (*func)();
    Renderable* r = new Renderable(m_window, &m_states, d);
    return r;
  }

  Renderable* RenderableFactory::make_renderable(int t,
						 std::vector<float> v)
  {
    RenderFunctor* func = m_functors[t];
    sf::Drawable* d = (*func)(v);
    Renderable* r = new Renderable(m_window, &m_states, d);
    return r;
  }
  
  Renderable* RenderableFactory::make_renderable(int t,
						 std::vector<sf::Vector2f> v)
  {
    RenderFunctor* func = m_functors[t];
    sf::Drawable* d = (*func)(v);
    Renderable* r = new Renderable(m_window, &m_states, d);
    return r;
  }

}

#endif
