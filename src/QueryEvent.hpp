#ifndef QUERYEVENT_HPP
#define QUERYEVENT_HPP

#include "EntityQuery.hpp"

namespace muncher
{
  struct QueryEvent
  {
    int id;
    int properties[50];
  };
  
}


#endif
