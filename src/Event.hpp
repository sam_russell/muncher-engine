#ifndef EVENT_HPP
#define EVENT_HPP

#include "EntityData.hpp"
#include "InputData.hpp"
#include "QueryData.hpp"
#include "EntityQuery.hpp"

namespace muncher
{  
  enum EventType
    {
     ENTITY = 1,
     INPUT = 2,
     QUERY = 3,
     QUIT = 4,
     QUERY_RESULT = 5
    };
  
  struct Event
  {
    long mtype;
    bool initialized;
    enum EventType type;
    union
    {
      EntityData entity;
      InputData input;
      QueryData query;
    };
    //Event() {}
  };
}


#endif
