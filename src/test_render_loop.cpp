#include <iostream>
#include <queue>
#include <unordered_map>
#include <vector>

#include <SFML/Graphics.hpp>

#include "RenderLoop.hpp"
#include "RenderFunctor.hpp"

namespace mu = muncher;

/* 
enum RenderFuncs
  {
   CIRCLE = 1,
   RECTANGLE = 2,
   CHAIN = 3
  };
*/

int main()
{
  

  std::unordered_map<int, mu::RenderFunctor*> funcs;
  funcs[mu::CIRCLE] = new mu::CircleFunctor(2.5f);
  funcs[mu::RECTANGLE] = new mu::RectangleFunctor(3.5f, 2.5f);
  funcs[mu::CHAIN] = new mu::ChainVertexArrayFunctor(5.0f);
  
  sf::RenderWindow* w = new sf::RenderWindow(sf::VideoMode(800, 600),
					     "FRUIT MUNCHER");

  mu::RenderLoop render_loop = mu::RenderLoop(w, funcs, 5.0f);

  return 0;
  
}
