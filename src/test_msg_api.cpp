#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include "Box2D/Box2D.h"
#include <SFML/Graphics.hpp>

#include "Event.hpp"

namespace mu = muncher;

int main()
{
  key_t key = ftok("some_file", 'c');
  //char* k = static_cast<char*>(key);

  std::cout << "key: " << key << std::endl;

  int mq_id = msgget(key, 0666 | IPC_CREAT);
  std::cout << "id: " << mq_id << std::endl;

  sf::Vector2f circle_r = sf::Vector2f(5.0f, 5.0f);

  mu::EntityData e1
    {1, // id
     1.0f, // restitution
     1.0f, // density
     mu::CIRCLE, // shape
     mu::DYNAMIC, // body type
     sf::Vector2f(10.0f, 20.0f), // position
     {circle_r} }; // dimensions
  mu::EntityData e2
    {2, // id
     1.0f, // restitution
     1.0f, // density
     mu::CHAIN, // shape
     mu::STATIC, // body type
     sf::Vector2f(0.0f, 0.0f), // position (0-init for chain)
     {sf::Vector2f(10.0f, 10.0f), // dimensions
      sf::Vector2f(15.0f, 15.0f),
      sf::Vector2f(20.0f, 15.0f),
      sf::Vector2f(30.0f, 11.0f)} };

  mu::Event* some_event;
  mu::Event ev1 {2, true, mu::ENTITY, e1};
  mu::Event ev2 {2, true, mu::ENTITY, e2};
  mu::Event ev3 {2, true, mu::QUIT, 0};


  mu::QueryData q {1, {mu::POSITION, mu::ANGLE} };
  mu::Event q1 {2, true, mu::QUERY};
  q1.query = q;


  *some_event = ev1;

  int sent1 = msgsnd(mq_id, &ev1, sizeof(ev1), 0);
  std::cout << "sent: " << sent1 << std::endl;

  int sent2 = msgsnd(mq_id, &ev2, sizeof(ev2), 0);
  std::cout << "sent: " << sent2 << std::endl;

  char quit;
  std::cout << "press 'q' to quit." << std::endl;
  std::cin >> quit;

  sf::Clock clock;

  mu::QueryResponse r {0};
  while (quit != 'q')
    {
      sf::Time elapsed = clock.getElapsedTime();
      std::cout << elapsed.asSeconds() << std::endl;
      sleep(0.0001f);
      
      if (quit == 'i')
	{
	  int queried = msgsnd(mq_id, &q1, sizeof(q1), 0);
	  std::cout << queried << std::endl;
	}

      int queried = msgsnd(mq_id, &q1, sizeof(q1), 0);
      int rec = msgrcv(mq_id, &r, sizeof(r), 1, IPC_NOWAIT);

      
      
      if (elapsed.asSeconds() > 1)
	std::cout << elapsed.asSeconds() << std::endl;
	{
	  if (rec != -1)
	    {
	      sf::Vector2f pos = r.result.position;
	      std::cout << pos.x << ", " << pos.y << std::endl;
	    }
	  clock.restart();
	}
	std::cin >> quit;
    }

  int sent3 = msgsnd(mq_id, &ev3, sizeof(ev3), 0);
  std::cout << "sent: " << sent3 << std::endl;
  std::cout << "ok bye now" << std::endl;
  
  return 0;
}
