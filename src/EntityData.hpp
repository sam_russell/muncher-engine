#ifndef ENTITYDATA_HPP
#define ENTITYDATA_HPP

#include <iostream>
#include <deque>

/*
#define CIRCLE 1
#define RECTANGLE 2
#define CHAIN 3

#define STATIC 1
#define DYNAMIC 2
*/

namespace muncher
{

  enum ShapeType
    {
     CIRCLE = 1,
     RECTANGLE = 2,
     CHAIN = 3
    };
  
  enum BodyType
    {	
     STATIC = 1,
     DYNAMIC = 2
    };

  struct EntityData
  {
    int id;
    float restitution;
    float density;
    int shape;
    int type;
    sf::Vector2f position;
    sf::Vector2f dimensions[100];
    float angle;
    float angular_velocity;
    sf::Vector2f linear_velocity;
  };

}

#endif
