#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include "Physics.hpp"

namespace mu = muncher;

int main(int argc, char* argv[])
{
  key_t k = atoi(argv[1]);

  mu::PhysicsManager pm = mu::PhysicsManager(k);
  pm.run();

  return 0;
}
