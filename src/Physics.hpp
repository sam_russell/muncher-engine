#ifndef PHYSICS_HPP
#define PHYSICS_HPP

#include <cmath>
#include <deque>
#include <queue>
#include <utility>
#include <sys/msg.h>
#include <stddef.h>


#include "Box2D/Box2D.h"
#include <SFML/Graphics.hpp>

#include "Event.hpp"
#include "EntityQuery.hpp"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#endif



namespace muncher
{
  //enum BodyType {STATIC, DYNAMIC};
  //enum FixtureShape {CIRCLE, RECTANGLE, POLYGON, CHAIN};

  class CircleFunctor
  {
  public:
    CircleFunctor() {}
    b2FixtureDef operator() (b2FixtureDef&, float);
  };
  b2FixtureDef CircleFunctor::operator() (b2FixtureDef& f, float radius)
  {
    b2CircleShape* circle = new b2CircleShape;
    circle->m_radius = radius;
    f.shape = circle;
    return f;
  }
  
  class RectangleFunctor
  {
  public:
    RectangleFunctor() {}
    b2FixtureDef operator() (b2FixtureDef&, float, float);
    b2FixtureDef operator() (b2FixtureDef&, sf::Vector2f);
  };
  
  b2FixtureDef RectangleFunctor::operator() (b2FixtureDef& f,
					     float w,
					     float h)
  {
    b2PolygonShape* rectangle = new b2PolygonShape;
    rectangle->SetAsBox(w / 2, h / 2);
    f.shape = rectangle;
    return f;
  }
  
  b2FixtureDef RectangleFunctor::operator() (b2FixtureDef& f,
					     sf::Vector2f v)
  {
    float w = v.x;
    float h = v.y;
    b2PolygonShape* rectangle = new b2PolygonShape;
    rectangle->SetAsBox(w / 2, h / 2);
    f.shape = rectangle;
    return f;
  }
  

  class ChainFunctor
  {
  public:
    ChainFunctor() {}
    b2FixtureDef operator() (b2FixtureDef&,
				      std::deque<sf::Vector2f>);
  };
  b2FixtureDef ChainFunctor::operator() (b2FixtureDef& f,
					 std::deque<sf::Vector2f> v)
  {
    int size = v.size();
    b2Vec2 verts[size];
    b2ChainShape* c = new b2ChainShape;
    for (int i = 0; i < size; ++i)
      {
	sf::Vector2f item = v[i];
	verts[i] = b2Vec2(item.x, item.y);
      }
    c->CreateChain(verts, size);
    f.shape = c;
    return f;
  }


  
  class PhysicsEntityBuilder
  {
  private:
    b2World* m_world;
  public:
    PhysicsEntityBuilder(b2World* world) : m_world{world} {}
    b2Body* make_body(int, int);
    b2Body* make_body(int, int, sf::Vector2f);
    template <class T, class F>
    b2Body* make(T,
		 float,
		 float,
		 int,
		 int);
    template <class T, class F>
    b2Body* make(T,
		 float,
		 float,
		 int,
		 int,
		 sf::Vector2f);
  };

  b2Body* PhysicsEntityBuilder::make_body(int type, int id)
  {
    b2BodyDef body_def;
    switch(type)
      {
      case STATIC:
	body_def.type = b2_staticBody;
	break;
      case DYNAMIC:
	body_def.type = b2_dynamicBody;
	break;
      }
    b2Body* body = m_world->CreateBody(&body_def);
    int* id_ptr = new int;
    *id_ptr = id;
    body->SetUserData(id_ptr);
    return body;
  }

  b2Body* PhysicsEntityBuilder::make_body(int type,
					  int id,
					  sf::Vector2f pos)
  {
    b2BodyDef body_def;
    switch(type)
      {
      case STATIC:
	body_def.type = b2_staticBody;
	break;
      case DYNAMIC:
	body_def.type = b2_dynamicBody;
	break;
      }
    body_def.position.Set(pos.x, pos.y);
    b2Body* body = m_world->CreateBody(&body_def);
    int* id_ptr = new int;
    *id_ptr = id;
    body->SetUserData(id_ptr);
    return body;
  }

  template <class T, class F>
  b2Body* PhysicsEntityBuilder::make(T dimensions,
				     float restitution,
				     float density,
				     int type,
				     int id)
				  
  {
    b2Body* body = this->make_body(type, id);
    b2FixtureDef fix_def;
    F functor = F();
    fix_def = functor(fix_def, dimensions);
    fix_def.density = density;
    fix_def.restitution = restitution;
    body->CreateFixture(&fix_def);
    return body;
  }

  template <class T, class F>
  b2Body* PhysicsEntityBuilder::make(T dimensions,
				     float restitution,
				     float density,
				     int type,
				     int id,
				     sf::Vector2f position)
  {
    b2Body* body = this->make_body(type, id, position);
    b2FixtureDef fix_def;
    F functor = F();
    fix_def = functor(fix_def, dimensions);
    fix_def.density = density;
    fix_def.restitution = restitution;
    body->CreateFixture(&fix_def);
    return body;
  }



  class PhysicsManager
  {
  private:
    int m_mq_id;
    key_t m_mq_key;
    PhysicsEntityBuilder* m_builder;
    b2World* m_world;
    std::queue<Event> m_inbox;
    bool m_quit;

    /* message queue methods */
    void listen();
    void stop();
    Event pop_event(Event&);

    /* entity CRUD */
    void create_entity(EntityData);
    void destroy_entity(int);
    EntityQuery* make_query(int[]);
    
    /* entity actions */
    void apply_force(int, float[2]);
    void apply_linear_impulse(int, float[2]);

    /* test methods */
    
    
  public:
    PhysicsManager(key_t);

    /* simulation */
    void run();
    EntityQuery* query_entity(int[], int);
  };

  PhysicsManager::PhysicsManager(key_t mq_key)
  {
    m_mq_key = mq_key;
    m_mq_id = msgget(m_mq_key, 0666 | IPC_CREAT);
    std::cout << m_mq_id << std::endl;

    m_world = new b2World( b2Vec2(0.0f, -10.0f) );
    m_builder = new PhysicsEntityBuilder(m_world);
  }

  void  PhysicsManager::listen()
  {
    // EntityData entity_msg;
    Event event {0};
    Event event_msg {0};
    // std::cout << "default init event" << std::endl;
    int rec = msgrcv(m_mq_id,
		     &event_msg,
		     sizeof(event_msg),
		     2,
		     IPC_NOWAIT);
    // std::cout << rec << std::endl;
    if (rec == -1)
      {
	if (errno != ENOMSG)
	  {
	    std::cout << "message queue error: " << errno << std::endl;
	  }
      }
    else
      {
	// std::cout << event_msg.type << std::endl;
	// std::cout << event_msg.entity.id << std::endl;
	memcpy(&event, &event_msg, sizeof(event_msg) );
	m_inbox.push(event);
	// std::cout << "pushed dereferenced event to queue" << std::endl;
      }
  }

  void PhysicsManager::stop()
  {
    msgctl(m_mq_id, IPC_RMID, NULL);
  }

  void PhysicsManager::create_entity(EntityData e)
  {
    const int sh = e.shape;
    switch (sh)
      {
      case CIRCLE:
	float radius;
	radius = e.dimensions[0].x;
	m_builder->template make<float, CircleFunctor>
	  (radius,
	   e.restitution,
	   e.density,
	   e.type,
	   e.id,
	   e.position);
	break;
      case RECTANGLE:
	m_builder->template make<sf::Vector2f, RectangleFunctor>
	  (e.dimensions[0],
	   e.restitution,
	   e.density,
	   e.type,
	   e.id,
	   e.position);
	break;
      case CHAIN:
	int num_edges = ARRAY_SIZE(e.dimensions);
	std::deque<sf::Vector2f> edges;
	for (int i = 0; i < num_edges; ++i)
	  {
	    /* excluding remaining 0-initialized 2d vectors 
	     (direction of x must be positive) */
	    if (i > 0
		&& e.dimensions[i + 1].x < e.dimensions[i].x)
	      {
		break;
	      }
	    else
	      {
		edges.push_back(e.dimensions[i]);
	      }
	  }
	m_builder->template
	  make<std::deque<sf::Vector2f>, ChainFunctor>
	  (edges,
	   e.restitution,
	   e.density,
	   e.type,
	   e.id);
	   //	   e.position);
	break;
      }
  }

  Event PhysicsManager::pop_event(Event& e)
  {
    e = m_inbox.front();
    m_inbox.pop();
    return e;
  }


  EntityQuery* PhysicsManager::make_query(int f_labels[])
  {
    std::vector<QueryFunctor*> funcs;
    int labels_len = ARRAY_SIZE(f_labels);
    for (int i = 0; i < labels_len; ++i)
      {
	/* removing 0-initialized nulls */
	if (f_labels[i] != 0)
	  {
	    funcs.push_back( GetQueryFunctor(f_labels[i]) );
	  }
      }
    EntityQuery* q = new EntityQuery(m_world, funcs);
    std::cout << "built query" << std::endl;
    return q;
  }

  EntityQuery* PhysicsManager::query_entity(int prop_labels[],
					    int id)
  {
    EntityQuery* query = make_query(prop_labels);
    return query->execute(id);
    //std::vector<EntityQueryResult> result = query.execute(id)->results()
  }

  void PhysicsManager::run()
  {
    while (m_quit == false)
      {
	sleep(0.0001f);
	this->listen();
	//std::cout << "listened" << std::endl;
	
	while ( !m_inbox.empty() )
	  {
	    // std::cout << "got a msg" << std::endl;
	    Event event {0};
	    event = this->pop_event(event);
	    
	    /* handle event */
	    
	    // just using a switch statement for now
	    int event_t = event.type;
	    EntityQuery* query;
	    QueryResponse response_msg {1};
	    int sent;
	    switch (event_t)
	      {
	      case ENTITY:
		this->create_entity(event.entity);
		std::cout << "Created entity " << event.entity.id
			  << std::endl;
		break;
	      case QUERY:
		std::cout << "query received" << std::endl;
		
		query = this->query_entity(event.query.properties,
						 event.query.id);
		std::cout << "query process completed" << std::endl;
		response_msg.result = query->results();
		sent = msgsnd(m_mq_id,
				  &response_msg,
				  sizeof(response_msg),
				  0);
		std::cout << "message status: " << sent << std::endl;
		break;
	      case QUIT:
		this->m_quit = true;
		std::cout << "quit message received" << std::endl;
		break;
	      }
	    
	    /* check for more messages */
	    // this->listen();
	  }
	
	m_world->Step(1.0f/20.0f, 10, 8);
	
	/* do stuff here */
	
      }
    this->stop();
  }

  
  
  

}



#endif
