#ifndef MANIFOLD_HPP
#define MANIFOLD_HPP

#include <deque>
#include <utility>
#include <vector>

#include <SFML/Graphics.hpp>

#define PI 3.141592


namespace muncher {

  float compute_normal_direction(sf::Vector2f e[2])
{
  float dx = abs(e[0].x - e[1].x);
  float dy = abs(e[0].y - e[1].y);
  float theta = atan(dy / dx);
  float degs = theta * 180.0f / PI;
  float degSupplement = 180.0f - degs;
  float radSupplement = degSupplement * PI / 180.0f;
  std::cout << "you compoted a normle, frend" << std::endl;
  return radSupplement;
}

  float compute_normal_direction(sf::Vector2f& v1, sf::Vector2f& v2)
{
  float dx = abs(v1.x - v2.x);
  float dy = abs(v1.y - v2.y);
  float theta = atan(dy / dx);
  float degs = theta * 180.0f / PI;
  std::cout << "theta: " << theta << std::endl;
  float degSupplement = 180.0f - degs;
  float radSupplement= degSupplement * PI / 180.0f;
  std::cout << "you compoted a normle, frend" << std::endl;
  return radSupplement;
}

  template <class T>
  std::pair<sf::Vector2f, sf::Vector2f> make_vertex_pair(T vertex,
							 float normal_dir,
							 float thickness)
  {
    std::pair<sf::Vector2f, sf::Vector2f> output;
    float dx = sin(normal_dir) * thickness;
    float dy = cos(normal_dir) * thickness;
    output = std::make_pair(sf::Vector2f(vertex.x, vertex.y),
			    sf::Vector2f(vertex.x + dx,
					 vertex.y + dy));
  }
  
  class Manifold
  {
  public:
    Manifold(float normalDir,
	     sf::Vector2f v0,
	     sf::Vector2f v1,
	     sf::Vector2f v2,
	     sf::Vector2f v3)
      : normalDir{normalDir}, v0{v0}, v1{v1}, v2{v2}, v3{v3}
    {
      // nop
    }
  
    float normalDir;
    sf::Vector2f v0;
    sf::Vector2f v1;
    sf::Vector2f v2;
    sf::Vector2f v3;
  
  };

  std::deque<Manifold> make_manifolds(std::vector<sf::Vector2f> chain,
				      float thickness)
  {
    std::deque<Manifold> manifolds;
    for (int i = 1; i < chain.size(); ++i)
      {
	sf::Vector2f edge[2] = {chain[i - 1], chain[i]};
	float normal_dir = compute_normal_direction(edge);
	std::pair<sf::Vector2f, sf::Vector2f> p1;
	std::pair<sf::Vector2f, sf::Vector2f> p2;
	p1 = make_vertex_pair<sf::Vector2f>(edge[0], normal_dir, thickness);
	p1 = make_vertex_pair<sf::Vector2f>(edge[1], normal_dir, thickness);
	Manifold m = Manifold(normal_dir,
			      p1.first,
			      p1.second,
			      p2.first,
			      p2.second);
	manifolds.push_back(m);
      }
    return manifolds;
  }

}

#endif
